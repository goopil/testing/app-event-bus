<?php

namespace App\Console\Commands;

use Enqueue\SimpleClient\SimpleClient;
use Faker\Factory;
use Illuminate\Console\Command;

class ProduceMessageCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'enqueue:message';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(SimpleClient $client, Factory $factory)
    {
        $faker = $factory->create();
        $howMuch = env('SENT_MESSAGE', 5);

        for ($i = 0; $i < $howMuch; $i++) {
            $client->sendEvent('enqueue_test', [
                'from'    => 'scheduler',
                'content' => $faker->paragraph(5),
            ]);

            if($i % 2 === 0) {
                $client->sendEvent('enqueue_test_2', [
                    'from'    => 'scheduler',
                    'content' => $faker->paragraph(5),
                ]);
            }
        }
    }
}
