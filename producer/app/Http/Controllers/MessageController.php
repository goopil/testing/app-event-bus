<?php

namespace App\Http\Controllers;

use Enqueue\SimpleClient\SimpleClient;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function store(Request $r, SimpleClient $client)
    {
        $client->setupBroker();
        $client->sendEvent('enqueue_test', [
            'from'    =>  $r->get('name', 'user'),
            'content' => $r->get('message'),
        ]);
    }
}
