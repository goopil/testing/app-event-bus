<?php

return [
    'client' => [
        'transport' => [
            'default' => 'amqp+bunny://guest:guest@rabbit:5672/%2f'
        ],
        'client'    => [
            'app_name' => 'producer'
        ],
    ],
];
