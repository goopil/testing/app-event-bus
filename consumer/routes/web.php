<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('info', 'UtilController@index');

Route::delete('messages/clear', 'MessageController@clear');

Route::get('stream', 'StreamController@index');

Route::get('/', 'MessageController@index');
