<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{csrf_token()}}">

    <title>{{env('APP_NAME')}}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{asset(mix('css/app.css'))}}">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .title {
            text-align: center;
            font-size: 84px;
        }
    </style>
</head>
<body>
<div id="app" class="flex-center">
    <div>
        <div class="title">{{env('APP_NAME')}}</div>
        <message-list :initial="{{ json_encode($messages) }}"></message-list>
    </div>
</div>

<script src="{{asset(mix('js/app.js'))}}"></script>
</body>
</html>
