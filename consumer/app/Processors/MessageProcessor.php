<?php

namespace App\Processors;

use Interop\Queue\PsrContext;
use Interop\Queue\PsrMessage;
use Interop\Queue\PsrProcessor;

class MessageProcessor implements PsrProcessor
{
    const topic = 'enqueue_test';

    protected $cacheKey = 'messages';

    public function process(PsrMessage $message, PsrContext $context)
    {
        $messages   = \Cache::get($this->cacheKey, []);
        $messages[] = [
            'body'      => json_decode($message->getBody()),
            'timestamp' => $message->getTimestamp(),
            'id'        => $message->getMessageId()
        ];

        \Cache::forever($this->cacheKey, $messages);

        return PsrProcessor::ACK;
    }
}
