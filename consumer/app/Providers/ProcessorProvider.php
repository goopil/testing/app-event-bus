<?php

namespace App\Providers;

use App\Processors\MessageProcessor;
use Enqueue\SimpleClient\SimpleClient;
use Illuminate\Support\ServiceProvider;

class ProcessorProvider extends ServiceProvider
{
    protected $processors = [
        MessageProcessor::class
    ];

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * @var $client SimpleClient
         */
        $client = $this->app->make(SimpleClient::class);

        foreach ($this->processors as $processor) {
            $client->bind($processor::topic, $processor, $this->app->make($processor));
        }
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
