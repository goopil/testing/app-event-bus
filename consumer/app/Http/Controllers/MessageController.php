<?php

namespace App\Http\Controllers;

class MessageController extends Controller
{
    public function index()
    {
        return view('welcome')->with([
            'messages' => \Cache::get('messages', [])
        ]);
    }

    public function clear()
    {
        \Cache::forget('messages');

        return response()->json([
            'success' => true
        ]);
    }
}
