<?php

namespace App\Http\Controllers;

class StreamController extends Controller
{
    public function index()
    {
        return response()->stream(function () {
            while (true) {
                $messages    = \Cache::get('messages', []);

                echo 'data:' . json_encode($messages) . "\n\n";
                ob_flush();
                flush();

                sleep(1);
            }
        }, 200, [
            'Content-Type'      => 'text/event-stream',
            'X-Accel-Buffering' => 'no',
            'Cache-Control'     => 'no-cache',
        ]);
    }
}
