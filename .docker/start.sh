#!/usr/bin/env bash

set -e

role=${CONTAINER_ROLE:-app}
env=${APP_ENV:-production}

if [ "$env" = "production" ]; then
    (cd /var/www/html && php artisan config:cache && php artisan route:cache && php artisan view:cache)
else
    (cd /var/www/html && php artisan config:clear && php artisan route:clear)
fi

if [ "$role" = "app" ]; then

    echo "Running the apache..."
    exec apache2-foreground

elif [ "$role" = "queue" ]; then

    echo "Running the queue..."
    php /var/www/html/artisan queue:work --verbose --tries=3 --timeout=90

elif [ "$role" = "consumer" ]; then

    echo "Running the consumer..."
    php /var/www/html/artisan enqueue:consume -vvv --setup-broker

elif [ "$role" = "scheduler" ]; then

    while [ true ]
    do
      echo "Running the scheduler..."
      php /var/www/html/artisan schedule:run --verbose --no-interaction &
      sleep 15
    done

else
    echo "Could not match the container role \"$role\""
    exit 1
fi
